package com.mobilisher.ac2012;

import java.util.Calendar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

public class Countdown2012Widget extends AppWidgetProvider {

	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		RemoteViews remoteViews = getActiveLayout(context);

		remoteViews.setImageViewBitmap(R.id.imTlYears,
				textToImg("YEARS", context, true));
		remoteViews.setImageViewBitmap(R.id.imTlMonths,
				textToImg("MONTHS", context, true));
		remoteViews.setImageViewBitmap(R.id.imTlDays,
				textToImg("DAYS", context, true));
		remoteViews.setImageViewBitmap(R.id.imTlHours,
				textToImg("HOURS", context, true));

		Calendar curDate = Calendar.getInstance();

		String yearsLeft = Integer.toString(2012 - curDate.get(Calendar.YEAR));
		String monthsLeft = Integer.toString(11 - curDate.get(Calendar.MONTH));
		String daysLeft = Integer.toString(curDate
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				- curDate.get(Calendar.DAY_OF_MONTH));
		String hoursLeft = Integer.toString(24 - curDate
				.get(Calendar.HOUR_OF_DAY));

		remoteViews.setImageViewBitmap(R.id.imCnYears,
				textToImg(yearsLeft, context, false));
		remoteViews.setImageViewBitmap(R.id.imCnMonths,
				textToImg(monthsLeft, context, false));
		remoteViews.setImageViewBitmap(R.id.imCnDays,
				textToImg(daysLeft, context, false));
		remoteViews.setImageViewBitmap(R.id.imCnHours,
				textToImg(hoursLeft, context, false));

		for (int i = 0; i < appWidgetIds.length; i++) {

			Intent active = new Intent(context, Preference.class);
			PendingIntent actionPendingIntent = PendingIntent.getActivity(
					context, 0, active, 0);
			remoteViews.setOnClickPendingIntent(R.id.lytMain,
					actionPendingIntent);

			appWidgetManager.updateAppWidget(appWidgetIds[i], remoteViews);

		}

		// appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);

		super.onUpdate(context, appWidgetManager, appWidgetIds);
	}

	private RemoteViews getActiveLayout(Context context) {
		String bcgrListPref = PreferenceManager.getDefaultSharedPreferences(
				context).getString("background_list_preference", "bc1");

		RemoteViews remoteViews = null;

		if (bcgrListPref.equals("bc"))
			remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget);
		else if (bcgrListPref.equals("bc1"))
			remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget1);
		else if (bcgrListPref.equals("bc2"))
			remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget2);
		else if (bcgrListPref.equals("bc3"))
			remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget3);
		else if (bcgrListPref.equals("bc4"))
			remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget4);
		else
			remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget1);

		return remoteViews;
	}

	private String getFontFace(Context context) {
		String fontListPref = PreferenceManager.getDefaultSharedPreferences(
				context).getString("font_list_preference", "Benegraphic.ttf");

		String fontFace;

		if (fontListPref.equals("AdventureSubtitles.ttf"))
			fontFace = "fonts/AdventureSubtitles.ttf";
		else if (fontListPref.equals("Benegraphic.ttf"))
			fontFace = "fonts/Benegraphic.ttf";
		else if (fontListPref.equals("BIRTH_OF_A_HERO.ttf"))
			fontFace = "fonts/BIRTH_OF_A_HERO.ttf";
		else if (fontListPref.equals("kberry.ttf"))
			fontFace = "fonts/kberry.ttf";
		else if (fontListPref.equals("kree.ttf"))
			fontFace = "fonts/kree.ttf";
		else
			fontFace = "fonts/Benegraphic.ttf";

		return fontFace;

	}

	private Bitmap textToImg(String text, Context context, boolean isTitle) {
		Bitmap btmpElement = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_4444);

		int curDensity = btmpElement.getDensity();
		int btmpElementHeight = 30 * curDensity / 160;
		int btmpElementWidth = 64 * curDensity / 160;

		btmpElement = Bitmap.createBitmap(btmpElementWidth, btmpElementHeight,
				Bitmap.Config.ARGB_4444);
		Canvas myCanvas = new Canvas(btmpElement);
		// myCanvas.drawColor(Color.RED);
		Log.i("font", getFontFace(context));
		Typeface tf = Typeface.createFromAsset(context.getAssets(),
				getFontFace(context));

		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setTypeface(tf);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.rgb(240, 240, 240));
		paint.setTextAlign(Align.CENTER);

		int txtSize;
		if (isTitle) {
			txtSize = 16 * curDensity / 160;

			paint.setTextSize(txtSize);
			myCanvas.drawText(text, btmpElementWidth / 2, txtSize + 10, paint);
		} else {
			txtSize = 30 * curDensity / 160;

			paint.setTextSize(txtSize);
			myCanvas.drawText(text, btmpElementWidth / 2, txtSize, paint);
		}

		return btmpElement;
	}
}