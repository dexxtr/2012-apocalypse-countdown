package com.mobilisher.ac2012;

import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.mobilisher.ac2012.services.AdMob;

public class Preference extends PreferenceActivity {

	private Dialog mIntallationDialog = null;

	public final static String PREFS_NAME = "ac2012Pref";
	public final static String TAG = "Preference";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

		if (!settings.getBoolean("notShowDialog", false)) {
			showInstallationDialog();
		}

		setAds();
	}

	/**
	 * Get control animation. Usually use as ad animation. 
	 * 
	 * @return Animation
	 */
	protected Animation getAnimation() {

		Animation anim = AnimationUtils.loadAnimation(this, R.anim.rotate);
		anim.reset();

		return anim;
	}

	/**
	 * Set ad to bottom of preference screen
	 */
	protected void setAds() {

		ViewGroup view = (ViewGroup) getWindow().getDecorView();
		LinearLayout content = (LinearLayout) view.getChildAt(0);

		AdMob ads = AdMob.getInstance(this);

		ads.setTargetLayout(content);
		ads.setAnimation(getAnimation());

		ads.show();
	}

	/**
	 * Show help or installation dialog for new users
	 */
	protected void showInstallationDialog() {

		mIntallationDialog = new Dialog(this);

		mIntallationDialog.setContentView(R.layout.installation);
		mIntallationDialog.setTitle(R.string.installation_dialog_label);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		boolean notShowDialog = settings.getBoolean("notShowDialog", false);

		CheckBox chShowDialog = (CheckBox) mIntallationDialog
				.findViewById(R.id.chShowDialog);
		chShowDialog.setChecked(notShowDialog);

		Button btnOk = (Button) mIntallationDialog.findViewById(R.id.ok);
		btnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mIntallationDialog.dismiss();

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();

				CheckBox chShowDialog = (CheckBox) mIntallationDialog
						.findViewById(R.id.chShowDialog);
				editor.putBoolean("notShowDialog", chShowDialog.isChecked());

				editor.commit();
			}
		});

		mIntallationDialog.show();
	}

	@Override
	public void onDestroy() {
		updateAllWidgets();
		AdMob.getInstance(this).destroy();
		
		super.onDestroy();
	}

	private void updateAllWidgets() {
		AppWidgetManager appWidgetManager = AppWidgetManager
				.getInstance(getApplicationContext());
		int[] appWidgetIds = appWidgetManager
				.getAppWidgetIds(new ComponentName(this,
						Countdown2012Widget.class));
		if (appWidgetIds.length > 0) {
			new Countdown2012Widget().onUpdate(this, appWidgetManager,
					appWidgetIds);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.help:
			showInstallationDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}