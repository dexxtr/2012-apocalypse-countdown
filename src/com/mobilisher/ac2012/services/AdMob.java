package com.mobilisher.ac2012.services;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

/**
 * @author dexter
 */
public class AdMob implements AdListener {

	private final static String TAG = "Ads";
	private final static String ADMOB_KEY = "a14cbac94548cf2";

	private static AdMob mInstance = null;

	private LinearLayout mTargetLayout = null;
	private Animation mAnimation = null;

	private AdView mAdView = null;

	private Activity mActivity = null;

	public static AdMob getInstance(Activity activity) {
		if (null == mInstance) {
			mInstance = new AdMob(activity);
		}

		return mInstance;
	}

	public AdMob(Activity activity) {
		mActivity = activity;
	}

	/**
	 * Set target layout which will be content ad
	 * 
	 * @param targetLayout
	 *            LinearLayout
	 */
	public void setTargetLayout(LinearLayout targetLayout) {
		mTargetLayout = targetLayout;

		Log.i(TAG, "Setted target layout");
	}

	/**
	 * Set ads animation resource. It'll be lunched while ads's showing on
	 * screen
	 */
	public void setAnimation(Animation animation) {
		mAnimation = animation;

		Log.i(TAG, "Setted animation resurce");
	}

	/**
	 * Load and show ad to target layout
	 */
	public void show() {

		if (!init() || mAdView == null) {
			return;
		}

		mAdView.loadAd(new AdRequest());
		Log.i(TAG, "Load ad to layout");
	}

	/**
	 * Destroy an ad object
	 */
	public void destroy() {

		if (mAdView != null) {
			mAdView.destroy();
			mAdView = null;
		}

		Log.i(TAG, "Destroy Ads instance");
	}

	/**
	 * Initialize and prepare ad for next load to layout
	 */
	protected boolean init() {

		if (null == mTargetLayout || !(mTargetLayout instanceof LinearLayout)) {
			Log.i(TAG,
					"mTargetLayout is null or doesn't support LinearLayout type");

			return false;
		}

		LinearLayout.LayoutParams lParam = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);

		mAdView = new AdView(mActivity, AdSize.BANNER, ADMOB_KEY);
		mAdView.setAdListener(this);
		mAdView.setGravity(Gravity.CENTER_HORIZONTAL);
		mAdView.setLayoutParams(lParam);

		mTargetLayout.addView(mAdView);

		Log.i(TAG, "Ad success initialized");

		return true;
	}

	/**
	 * Called when an ad is received
	 */
	public void onReceiveAd(Ad ad) {

		if (ad.isReady() && mAnimation != null) {

			mAdView.clearAnimation();
			mAdView.startAnimation(mAnimation);

			Log.i(TAG, "Animation started");
		}
	}

	/**
	 * Called when an ad was not received
	 */
	public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {

		switch (error) {
			case INTERNAL_ERROR:
				Log.i(TAG, "Something happened internally; for instance, the Activity may have been destroyed mid-refresh");
				break;
				
			case INVALID_REQUEST:
				Log.i(TAG, "The ad request is invalid");
				break;
				
			case NETWORK_ERROR:
				Log.i(TAG, "The ad request was unsuccessful due to network connectivity");
				break;
				
			case NO_FILL:
				Log.i(TAG, "The ad request is successful, but no ad was returned due to lack of ad inventory");
				break;
				
			default:
				Log.i(TAG, "Undefined error occurred");
				break;
		}
		
	}

	/**
	 * Called when an Activity is created in front of the app
	 */
	public void onPresentScreen(Ad ad) {

	}

	/**
	 * Called when an ad is clicked and about to return to the application.
	 */
	public void onDismissScreen(Ad ad) {

	}

	/**
	 * Called when an ad is clicked and going to start a new Activity that will
	 * leave the application
	 */
	public void onLeaveApplication(Ad ad) {

	}
}